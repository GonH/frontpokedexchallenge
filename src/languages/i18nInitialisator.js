import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { getBrowserLanguage, saveLang } from '../helpers/languageUtils';

const EN = require('./en.i18n.json');
const ES = require('./es.i18n.json');

const i18nInitialise = () => {
  const defaultLang = getBrowserLanguage();

  saveLang(defaultLang);

  return i18n.use(initReactI18next).init({
    lng: defaultLang,
    resources: {
      en: {
        translation: EN
      },
      es: {
        translation: ES
      }
    },
    fallbackLng: defaultLang
  });
};

export default i18nInitialise;
