import React from "react";
import ReactDOM from "react-dom";
import Logger from "./helpers/Logger";
import i18nInitialise from "./languages/i18nInitialisator";

i18nInitialise()
  .then(() => {
    ReactDOM.render(
      <label>Its just an empty web</label>,
      document.getElementById("root")
    );
  })
  .catch(error => {
    Logger.error("[index.i18nInitialise] Error while initialising i18n", error);
    ReactDOM.render(<div />, document.getElementById("root"));
  });
